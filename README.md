# Trilobite printer

NOTE: This is a canceled project, archived in case it's ever useful. 

A desktop delta printer, strong and fast.

(not ready for prime-time, though :-)

![A screenshot of a very incomplete CAD drawing of a delta printer.](https://codeberg.org/Transmutable/trilobite-printer/raw/commit/dfc3ee9078f44e0206bfd195e859acd766d3dc18/images/trilobite-printer-screenshot.png)

## License

[CERN Open Hardware Licence Version 2 - Strongly Reciprocal](https://ohwr.org/cern_ohl_s_v2.txt)
